-- drop table if exists switchs;
-- CREATE TABLE switchs
-- (
-- id integer primary key autoincrement,
-- name VARCHAR(100),
-- code_on VARCHAR(100),
-- code_off VARCHAR(100)
-- );
-- drop table if exists cronAction;
-- CREATE TABLE cronAction
-- (
-- id integer primary key autoincrement,
-- label  VARCHAR(100),
-- onhour integer NOT NULL,
-- onminute integer NOT NULL,
-- offhour integer NOT NULL,
-- offminute integer NOT NULL,
-- dayofweek  VARCHAR(100),
-- active boolean DEFAULT True,
-- switch_id INTEGER REFERENCES switchs(id) ON delete CASCADE
-- );
drop table if exists webRadio;
CREATE TABLE webRadio
(
id integer primary key autoincrement,
name  VARCHAR(100),
url integer NOT NULL,
selected boolean DEFAULT False,
playing boolean DEFAULT False
);
