$(function() {
  $("#message").hide();
});



function dayClicked(dayButton){
  var btn = document.getElementById(dayButton);
  if (btn.className== "btn btn-default"){
    btn.className= "btn btn-primary";
  }else{
    btn.className= "btn btn-default";
  }
}

function goAddAlarmClock(){
  // get all selection
  var label   = document.getElementById("label").value;
  var onhour    = document.getElementById("onhour").value;
  var onminute  = document.getElementById("onminute").value;
  var offhour    = document.getElementById("offhour").value;
  var offminute  = document.getElementById("offminute").value;
  var bouton = document.getElementById("bouton").value;
  var dayofweek="";
  var first = 0; // boolean to detect the first insert
  var table = ["suButton", "moButton","tuButton","weButton","thButton","frButton","saButton"];
  for (var i=0;i<table.length;i++){
    var dayButton = document.getElementById(table[i]);
    if(dayButton.className == "btn btn-primary"){
      if (first == 0){ // if it's the fisrt time we add a number no float
        dayofweek+= i.toString();
        first =1;   // we found one
      }else{
        dayofweek+= ","+i.toString();
      }
    }
  }

  // send all of that shit to the server
  var request = $.ajax({
    type: "POST",
    url: "/addCronAction/",
    dataType: "json",
    traditional: true,
    data: {
      label: label,
      onhour: onhour,
      onminute: onminute,
      offhour: offhour,
      offminute: offminute,
      bouton: bouton,
      dayofweek: dayofweek
    },
    success:  window.location.href = "/cronActionsList/"
  });
}
