#Home Alone

Home Alone is a personnal Home website that allow you :

* Manage your electric device by RF
* to broadcast a radio on your bluetooth speaker
* to schedule to turn on or off the electric device of your home

![home](http://pictuploader.traquenard.net/assets/products/141/middle/S%C3%A9lection_999%28236%29.png)

# Prerequisit

## Install  WiringPi library doing :


    cd ~/
    git clone git://git.drogon.net/wiringPi
    cd wiringPi
    ./build


## Install  433Utils library doing :


    cd ~/
    git clone --recursive git://github.com/ninjablocks/433Utils.git
    cd 433Utils/RPi_utils
    make


## Use the code of this RFSniffer to get the ID of your electric outlet if you are using otio

https://github.com/timleland/rfoutlet


## Install git, mplayer, python, ...

    sudo apt-get install python-pip mplayer  sqlite3 git python-alsaaudio python-virtualenv

# how to install

## clone repo :

git clone git@bitbucket.org:leotrouvtou/home-alone.git

## go in the folder and create a virtualenv


    cd home-alone
    virtualenv  venv
    source venv/bin/activate


## Install requirements

`pip install -r requirements.txt`

## run server

`python server.py`


A huge thanks to https://github.com/Sispheor/Piclodio2  that did basicaly everything for me
