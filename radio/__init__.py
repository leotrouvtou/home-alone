import re

class Radio:
    """
    Radio class.
    Allow to create, remove, disable an enable a Linux crontab line
    """

    def __init__(self):
        self.name = ''
        self.url = ''
        self.selected = False
        self.playing= False
