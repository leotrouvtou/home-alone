import os
import time
from flask import Flask
from flask import render_template
from flask import redirect, url_for
from flask import request, session
import subprocess
from os.path import expanduser
import sqlite3 as sql
from crontab import Crontab
from player import Player
from radio import Radio
from time import strftime
import urllib
from flask_oauth import OAuth
from functools import wraps

# You must configure these 3 values from Google APIs console
# https://code.google.com/apis/console
GOOGLE_CLIENT_ID = 'GOOGLE_CLIENT_ID'
GOOGLE_CLIENT_SECRET = 'GOOGLE_CLIENT_SECRET'
REDIRECT_URI = '/oauth2callback'  # one of the Redirect URIs from Google APIs console
SECRET_KEY = 'developpement'
DEBUG = True

app = Flask(__name__, instance_path='/home/leotrouvtou/src/home-alone')

#app.config.from_object('app.default_settings')
app.config.from_pyfile('local_settings.cfg')


GOOGLE_CLIENT_ID = app.config['GOOGLE_CLIENT_ID']
GOOGLE_CLIENT_SECRET = app.config['GOOGLE_CLIENT_SECRET']
SECRET_KEY = app.config['SECRET_KEY']
# Configure database
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'flaskr.db'),
))


pulse="700"
protocol="2"

app.debug = DEBUG
app.secret_key = SECRET_KEY
oauth = OAuth()
 
google = oauth.remote_app('google',
                          base_url='https://www.google.com/accounts/',
                          authorize_url='https://accounts.google.com/o/oauth2/auth',
                          request_token_url=None,
                          request_token_params={'scope': 'https://www.googleapis.com/auth/userinfo.email',
                                                'response_type': 'code'},
                          access_token_url='https://accounts.google.com/o/oauth2/token',
                          access_token_method='POST',
                          access_token_params={'grant_type': 'authorization_code'},
                          consumer_key=GOOGLE_CLIENT_ID,
                          consumer_secret=GOOGLE_CLIENT_SECRET)
listEmail = ['lebernard@gmail.com', 'cecilianivet@gmail.com']

def requires_auth(f):
  @wraps(f)
  def decorated(*args, **kwargs):
      access_token = session.get('access_token')
      if access_token is None:
          return redirect(url_for('login'))
      
      access_token = access_token[0]
      from urllib2 import Request, urlopen, URLError
      
      headers = {'Authorization': 'OAuth '+access_token}
      req = Request('https://www.googleapis.com/oauth2/v1/userinfo',
                    None, headers)
      try:
          res = urlopen(req)
          result = res.read()
      except URLError, e:
          if e.code == 401:
              # Unauthorized - bad token
              session.pop('access_token', None)
              return redirect(url_for('login'))
          return result
      for email in listEmail:
          if email in result:
              return f(*args, **kwargs)
      return "desole"
  return decorated

@app.route('/login')
def login():
    callback=url_for('authorized', _external=True)
    return google.authorize(callback=callback)

@app.route(REDIRECT_URI)
@google.authorized_handler
def authorized(resp):
    access_token = resp['access_token']
    session['access_token'] = access_token, ''
    return redirect(url_for('index'))

@google.tokengetter
def get_access_token():
    return session.get('access_token')

def request_db(fd, params=()):
    con = sql.connect(app.config['DATABASE'])
    con.row_factory = lambda c, r: dict([(col[0], r[idx]) for idx, col in enumerate(c.description)])
    #sql.Row
    cur = con.cursor()
    with open(fd) as fd:
        try:
            cur.execute(fd.read(), params)
            return cur.fetchall()
        except Exception as e:
            con.rollback()
            raise
    con.close()

def write_db(fd, params=None):
    con = sql.connect(app.config['DATABASE'])
    con.row_factory = sql.Row
    cur = con.cursor()
    with open(fd) as fd:
        try:
            cur.execute(fd.read(), params)
            con.commit()
        except Exception as e:
            con.rollback()
            raise
    con.close()

def sendSignal(code):
    subprocess.call("~/433Utils/RPi_utils/codesend {code} {protocol} {pulse}".format(code=code, protocol=protocol, pulse=pulse), shell=True)

def enable(cronAction, button):
    """
    enable The alarm clock. Set it into the crontab
    """
    base_dir = os.path.dirname(os.path.dirname(__file__))
    cronOn = Crontab()
    cronOn.minute = cronAction['onminute']
    cronOn.hour = cronAction['onhour']
    cronOn.period = cronAction['dayofweek']
    cronOn.comment = "piclodio ON "+str(cronAction['id'])
    cronOn.command = "~/433Utils/RPi_utils/codesend "+button['code_on']+" "+protocol+" "+pulse
    cronOn.create()
    cronOff = Crontab()
    cronOff.minute = cronAction['offminute']
    cronOff.hour = cronAction['offhour']
    cronOff.period = cronAction['dayofweek']
    cronOff.comment = "piclodio OFF "+str(cronAction['id'])
    cronOff.command = "~/433Utils/RPi_utils/codesend "+button['code_off']+" "+protocol+" "+pulse
    cronOff.create()

def disable(id):
    """
    disable the alarm clock. remove it from the crontab
    """
    cronOn = Crontab()
    cronOn.comment = "piclodio ON "+str(id)
    cronOn.remove()
    cronOff = Crontab()
    cronOff.comment = "piclodio OFF "+str(id)
    cronOff.remove()

def _convert_period_to_crontab(period):
    # transform period into crontab compatible
    period_crontab = ""
    first_time = True  # first time we add a value
    for p in period.split(','):
        if first_time:  # we do not add ","
            period_crontab += str(p)
            first_time = False
        else:
            period_crontab += ","
            period_crontab += str(p)
    return period_crontab

def is_valid(form):
    choice_hour = [(str(i), i) for i in range(0, 24)]
    choice_minute = [(str(i), i) for i in range(0, 60)]
    choice_period = [('1', 'Mon'),
                     ('2', 'Tue'),
                     ('3', 'Wed'),
                     ('4', 'Thu'),
                     ('5', 'Fri'),
                     ('6', 'Sat'),
                     ('0', 'Sun')]
    if form['onhour'] in choice_hour:
        print "onhour"
        return False
    if form['onminute'] in choice_minute:
        print "onminute"
        return False
    if form['offhour'] in choice_hour:
        print "offhour"
        return False
    if form['offminute'] in choice_minute:
        print "offminute"
        return False
    for i in form['dayofweek'].split(','):
        if i in choice_period:
            return False
    return True

@app.route("/cronActionsList/")
@requires_auth
def cronActionsList():
    list_cron = request_db('sql/list_cron.sql')
    return render_template('cronAction.html', listAlarm=list_cron)

@app.route("/addCronAction/", methods=['GET', 'POST'])
@requires_auth
def addCronAction():
    if request.method=="GET":
        buttonsList = request_db('sql/list_switchs.sql')
        return render_template('addCronAction.html', buttonsList=buttonsList)
    elif request.method=="POST":
        form =request.form # A form bound to the POST data
        button = request_db('sql/detail_switch.sql', (form["bouton"],))
        if is_valid(form):  # All validation rules pass
            # convert period
            period = form['dayofweek']
            form.dayofweek = _convert_period_to_crontab(period)
            form.active = 1
            # save in database
            write_db('sql/createCronAction.sql', (form["label"], form["onhour"], form["onminute"], form["offhour"], form["offminute"], form.dayofweek, form.active, form["bouton"]))
            form=request_db('sql/getLastCronAction.sql')
            # set the cron
            enable(form[0], button[0])
            return redirect(url_for('cronActionsList'))
        else:
            form = request.form  # An unbound form
        return render(request, 'addCronAction.html', {'form': form})

@app.route("/")
@requires_auth
def index():
    clock = strftime("%H:%M:%S")
    switchs = request_db('sql/list_switchs.sql')
    crons = request_db('sql/list_cron.sql')
    radios = request_db('sql/list_webRadio.sql')
    return render_template('simple.html', switchs=switchs,listAlarm=crons, clock=clock, radios=radios)

@app.route("/admin/")
@requires_auth
def admin():
    clock = strftime("%H:%M:%S")
    switchs = request_db('sql/list_switchs.sql')
    crons = request_db('sql/list_cron.sql')
    radios = request_db('sql/list_webRadio.sql')
    return render_template('index.html', switchs=switchs,listAlarm=crons, clock=clock, radios=radios)

@app.route("/simpleSwitchList/")
@requires_auth
def simpleSwitchList():
    switchs = request_db('sql/list_switchs.sql')
    return render_template('switchList.html', switchs=switchs)

@app.route("/cronActionsList/delete/<id>")
@requires_auth
def delete_cronAction(id):
    disable(id)
    write_db('sql/deleteCronAction.sql', (id,))
    return redirect(url_for('cronActionsList'))

@app.route("/cronActionsList/activate/<id>")
@requires_auth
def activate(id):
    write_db('sql/activateCronAction.sql', (id,))
    return redirect(url_for('cronActionsList'))

@app.route("/send_code/<code>")
@requires_auth
def send_code(code):
    sendSignal(code)
    return redirect(url_for('index'))



@app.route("/addwebradio/", methods=['GET', 'POST'])
@requires_auth
def addwebradio():
    if request.method=="POST":  # If the form has been submitted...
        form = request.form  # A form bound to the POST data
        # save web radio
        write_db('sql/addWebRadio.sql', (form['label'], form['url']))
    else:
        return render_template('addwebradio.html')
    return redirect(url_for('webradio'))

@app.route("/webradio/delete/<id>")
@requires_auth
def delete_webradio(id):
    write_db('sql/deleteWebRadio.sql', (id,))
    return redirect(url_for('webradio'))




@app.route("/webradio/")
@requires_auth
def webradio():
    listradio = request_db('sql/list_webRadio.sql')
    return render_template('webradio.html', listradio=listradio)

@app.route("/simpleRadioList/")
@requires_auth
def simpleradiolist():
    listradio = request_db('sql/list_webRadio.sql')
    return render_template('simpleradiolist.html', listradio=listradio)

@app.route("/play/<id_radio>")
@requires_auth
def play(id_radio):
    # set the new selected radio
    radio_db =  request_db('sql/detail_radio.sql', (id_radio,))
    radio = Radio()
    radio.name = radio_db[0]['name']
    radio.url = radio_db[0]['url']
    radio.id = id_radio
    radio.selected = radio_db[0]['selected']
    radio.selected = '1'
    radio.playing = '1'
    write_db('sql/unselectallwebradio.sql',('0','0') )
    write_db('sql/updatewebradio.sql', (radio.name, radio.url, radio.selected, radio.playing, id_radio))
    player = Player()
    # check if url is available
    try:
        http_code = urllib.urlopen(radio.url).getcode()
    except IOError:
        http_code = 0
    print http_code
    if http_code == 200:
       sendSignal('969827841')
       player.play(radio)
    else:
        # play backup MP3
        radio.url = 'mplayer backup_mp3/*'
        player.play(radio.url)
    return redirect('/simpleRadioList/')

@app.route("/stop/")
@requires_auth
def stop():
    player = Player()
    player.stop()
    sendSignal('902718977')
    write_db('sql/unplayallwebradio.sql', ('0',))
    time.sleep(1)
    return redirect('/simpleRadioList/')

@app.route("/reloadSound/")
@requires_auth
def reloadSound():
    p = subprocess.Popen("~/blue.sh &", shell=True)
    p.communicate()
    time.sleep(1)
    return redirect('/simpleRadioList/')


if __name__ == "__main__":
    #app.run(host="192.168.0.19")
    #app.run(host="169.254.231.66")
    app.run(host="localhost", port=5005)

